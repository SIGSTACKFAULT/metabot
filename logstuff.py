import asyncio
import logging
from logging import Handler, Formatter
from typing import Optional

import time

import aiohttp
import discord

log = logging.getLogger(__name__)

class DiscordHandler(Handler):
    """Send messages to a discord webhook."""
    def __init__(self, url:Optional[str], level=logging.INFO):
        super().__init__(level)
        self.url = url
        if self.url is None:
            log.warning("No url passed, webhook will not work.")
            return
        self.formatter = logging.Formatter("**[{levelname}]** {name}: {message}", style='{')
        self._session = aiohttp.ClientSession(
            cookie_jar = aiohttp.DummyCookieJar()
        )
        self._queue = asyncio.Queue()
        self._task = None
        self.start()
    
    def start(self):
        if self._task is None or self._task.done():
            self._task = asyncio.create_task(self._bgtask())
    
    def emit(self, record:logging.LogRecord):
        if self.url is None: return
        # don't try and log our own messages
        if record.name == __name__: return

        # if the task quit, start it
        if self._task.done(): self.start()
        self._queue.put_nowait(record)
    
    async def _bgtask(self):
        if self.url is None: return
        """Handle messages from the queue."""
        while True:
            record:logging.LogRecord = await self._queue.get()
            msg = self.formatter.format(record)

            while True:
                async with self._session.post(
                    self.url,
                    json={"content":msg}
                ) as resp:
                    if resp.status//100 == 2:
                        # 2xx status - we're good!
                        break

                    json = await resp.json()

                    if resp.status == 429:
                        # rate limit
                        timeout = json["retry_after"]
                        log.warn(f"rate limit {timeout}ms")
                        timeout += 10 # to be safe
                        await asyncio.sleep(timeout/1000)
                        continue
                    else:
                        log.warn(f"POST returned {resp.status} {resp.reason} {json}")
                    break

            self._queue.task_done()
