import subprocess

version = "2.0.1"

try:
    description = subprocess.check_output(["git", "describe", "--dirty"], stderr=subprocess.DEVNULL).decode().strip()
except:
    description = None
